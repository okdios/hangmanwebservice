/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.okdios.hangman_web;


import java.io.*;
import java.util.Random;
import java.util.Vector;
/**
 *
 * @author okd
 */
public class WordGenerator {
    String file;
    Vector wordlist;
    Debug debug;
    public WordGenerator(Debug debug)
    {
        this.debug = debug;
        //String base = System.getProperty("user.dir");
        this.file = System.getProperty("user.dir") + "/SKOLE/hangman_wordlist.txt";
        this.createWordlist();
    }
    
    private void createWordlist()
    {
        File f = new File(this.file);
        if(f.exists()) 
        {
            this.readFile();
        }
        else
        {
            this.debug.print("Could not find file with words in " + this.file + ". Adding a few "
                           + "words manually to make the program work.");
            Vector words = new Vector();
            words.add("computer");
            words.add("hangman");
            words.add("modem");
            this.wordlist = words;
        }
    }
    
    private void readFile()
    {
        String ln;
        String[] apps;
        Vector lines = new Vector();

        try
        {
            FileReader fr=new FileReader(this.file);
            BufferedReader br=new BufferedReader(fr);

            while((ln=br.readLine())!=null)
            {
                lines.addElement(ln);
                this.debug.print("Adding the word: " + ln);
            }

            apps = new String[lines.size()];

            for (int i = 0; i < apps.length; i++)
            {
                apps[i] = (String) lines.elementAt(i);
            }
            this.wordlist = lines;
        }
        catch(IOException e)
        {
            this.debug.print("Error: " + e);
        }

    }
    
    
    public String getRandomWord()
    {
        return this.wordlist.get(this.getRandomNumber()).toString();
    }
    
    
    private int getRandomNumber()
    {
        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(this.wordlist.size());
        return randomInt;
    }
}

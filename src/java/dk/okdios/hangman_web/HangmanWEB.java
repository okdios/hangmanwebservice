package dk.okdios.hangman_web;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
//import javax.ejb.Stateless;

/**
 *
 * @author okd
 */
@WebService(serviceName = "WebApplicationWS1")
//@Stateless()
public class HangmanWEB {

    Hangman hangman;
    
    public HangmanWEB()
    {
        this.hangman = new Hangman();
    }
    
    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "hello")
    public String hello(@WebParam(name = "name") String txt) {
        return "Hello " + txt + " !";
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "restartGame")
    public void restartGame() {
        this.hangman.debug.print("starting a new game");
        this.hangman.newGame();
    }

    /**
     * Web service operation
     */
    /****************** TEST *********************************************/
    @WebMethod(operationName = "test_getWord")
    public String test_getWord() {
        this.hangman.debug.print("TEST: get a random word");
        return this.hangman.wordGenerator.getRandomWord();
        
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "checkLetter")
    public boolean checkLetter(@WebParam(name = "letter") String letter) {
        this.hangman.debug.print("Checking the letter: " + letter);
        return this.hangman.checkLetter(letter);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "isGameOver")
    public boolean isGameOver() {
        this.hangman.debug.print("Checking if game is over");
        return this.hangman.isGameOver();
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "won")
    public boolean won() {
        this.hangman.debug.print("Checking if user has won");
        return this.hangman.won();
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getFoundAsString")
    public String getFoundAsString() {
        this.hangman.debug.print("Getting string with found letters");
        return this.hangman.getFoundAsString();
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getWordAsString")
    public String getWordAsString() {
        this.hangman.debug.print("Getting string with the word that should be guessed");
        return this.hangman.getWordAsString();
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getRemainingAttempts")
    public int getRemainingAttempts() {
        this.hangman.debug.print("Getting remaining attempts as int");
        return this.hangman.getRemainingAttempts();
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getRemainingAttemptsAsString")
    public String getRemainingAttemptsAsString() {
        this.hangman.debug.print("Getting remaining attempts as string");
        return this.hangman.getRemainingAttemptsAsString();
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getUsedLettersAsString")
    public String getUsedLettersAsString() {
        this.hangman.debug.print("Getting string with list of used letters");
        return this.hangman.getUsedLettersAsString();
    }
}
